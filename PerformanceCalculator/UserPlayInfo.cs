// Copyright (c) ppy Pty Ltd <contact@ppy.sh>. Licensed under the MIT Licence.
// See the LICENCE file in the repository root for full licence text.

using System;
using osu.Game.Beatmaps;

namespace PerformanceCalculator
{
    /// <summary>
    /// Holds the live pp value, beatmap name, and mods for a user play.
    /// </summary>
    public class UserPlayInfo
    {
        public double LocalPP;
        public double LivePP;
        public double WeightedPP;
        public double MissCount;
        public double Accuracy;
        public DateTimeOffset Date;

        public BeatmapInfo Beatmap;

        public string[] Mods;
        public int Combo;
        public int MaxCombo;
    }
}
