using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Alba.CsConsoleFormat;
using JetBrains.Annotations;
using McMaster.Extensions.CommandLineUtils;
using Newtonsoft.Json;
using osu.Game.Rulesets.Mods;
using osu.Game.Rulesets.Scoring;
using osu.Game.Scoring;
using Realms;

namespace PerformanceCalculator.LocalProfile
{
    [Command(Name = "local-profile", Description = "Computes the total performance (pp) of a local profile.")]
    public class LocalProfileCommand : ProcessorCommand
    {
        [UsedImplicitly]
        [Required]
        [Argument(0, Name = "user", Description = "Username")]
        public string ProfileName { get; }

        [UsedImplicitly]
        [Required]
        [Argument(1, Name = "path", Description = "Required. The path to the osu!lazer program files.")]
        public string osuPath { get; }

        [UsedImplicitly]
        [Option(Template = "-r|--ruleset:<ruleset-id>", Description = "The ruleset to compute the profile for.\n"
                                                                      + "Values: 0 - osu!, 1 - osu!taiko, 2 - osu!catch, 3 - osu!mania")]
        [AllowedValues("0", "1", "2", "3")]
        public int Ruleset { get; } = 0;

        [UsedImplicitly]
        [Option(Template = "-j|--json", Description = "Output results as JSON.")]
        public bool OutputJson { get; }

        public override void Execute()
        {
            var displayPlays = new List<UserPlayInfo>();
            var ruleset = LegacyHelper.GetRulesetFromLegacyID(Ruleset);

            Console.WriteLine("Getting scores...");
            var realmPath = Path.Join(osuPath, "client.realm");
            var config = new RealmConfiguration(realmPath)
            {
                IsReadOnly = true,
                SchemaVersion = 25
            };
            var realm = Realm.GetInstance(config).Freeze();

            var scores = realm.All<ScoreInfo>().Filter($"Ruleset.OnlineID == {Ruleset} && User.Username == '{ProfileName}'");

            Console.WriteLine("Calculating PP...");

            foreach (var scoreInfo in scores)
            {
                var replayHash = scoreInfo.Hash;
                var beatmapHash = scoreInfo.BeatmapInfo.Hash;

                var replayPath = Path.Join(osuPath, "files", replayHash.Substring(0,1), replayHash.Substring(0,2), replayHash);
                var beatmapPath = Path.Join(osuPath, "files", beatmapHash.Substring(0,1), beatmapHash.Substring(0,2), beatmapHash);

                var workingBeatmap = new ProcessorWorkingBeatmap(beatmapPath);
                var scoreParser = new ProcessorScoreDecoder(workingBeatmap);
                var difficultyCalculator = ruleset.CreateDifficultyCalculator(workingBeatmap);

                Mod[] mods = scoreInfo.Mods;
                if (scoreInfo.IsLegacyScore)
                    mods = LegacyHelper.ConvertToLegacyDifficultyAdjustmentMods(ruleset, mods);

                var difficultyAttributes = difficultyCalculator.Calculate(mods);
                var performanceCalculator = ruleset.CreatePerformanceCalculator();

                var ppAttributes = performanceCalculator?.Calculate(scoreInfo, difficultyAttributes);

                var thisPlay = new UserPlayInfo
                {
                    Beatmap = workingBeatmap.BeatmapInfo,
                    LocalPP = ppAttributes?.Total ?? 0,
                    Mods = scoreInfo.Mods.Select(m => m.Acronym).ToArray(),
                    MissCount = scoreInfo.Statistics.GetValueOrDefault(HitResult.Miss),
                    Accuracy = scoreInfo.Accuracy * 100,
                    Date = scoreInfo.Date.ToLocalTime(),
                    Combo = scoreInfo.MaxCombo,
                    MaxCombo = difficultyAttributes.MaxCombo
                };

                displayPlays.Add(thisPlay);
            }

            var topPlays = displayPlays.GroupBy(p => p.Beatmap.OnlineID).Select(group => group.OrderByDescending(p => p.LocalPP).First());
            var localOrdered = topPlays.OrderByDescending(p => p.LocalPP).Take(100).ToList();

            double totalLocalPP = 0;
            int index = 0;
            foreach (var play in localOrdered)
            {
                play.WeightedPP = Math.Pow(0.95, index++) * play.LocalPP;
                totalLocalPP += play.WeightedPP;
            }

            var playCount = topPlays.Count();
            var playCountPP = 416.6667 * (1 - Math.Pow(0.9994, playCount));
            totalLocalPP += playCountPP;

            if (OutputJson)
            {
                var json = JsonConvert.SerializeObject(new
                {
                    Username = ProfileName,
                    LocalPp = totalLocalPP,
                    PlaycountPp = playCountPP,
                    Scores = localOrdered.Select(item => new
                    {
                        BeatmapId = item.Beatmap.OnlineID,
                        BeatmapName = item.Beatmap.ToString(),
                        item.Date,
                        item.Combo,
                        item.Accuracy,
                        item.MissCount,
                        item.Mods,
                        LocalPp = item.LocalPP,
                        WeightedPp = item.WeightedPP
                    })
                });

                Console.Write(json);

                if (OutputFile != null)
                    File.WriteAllText(OutputFile, json);
            }
            else
            {
                OutputDocument(new Document(
                    new Span($"User:     {ProfileName}"), "\n",
                    new Span($"Local PP: {totalLocalPP:F1} (including {playCountPP:F1}pp from {playCount} completed maps)"), "\n",
                    new Grid
                    {
                        Columns =
                        {
                            GridLength.Auto, GridLength.Auto, GridLength.Auto, GridLength.Auto, GridLength.Auto, GridLength.Auto, GridLength.Auto, GridLength.Auto, GridLength.Auto
                        },
                        Children =
                        {
                            new Cell("#"),
                            new Cell("beatmap"),
                            new Cell("date"),
                            new Cell("max combo"),
                            new Cell("accuracy"),
                            new Cell("misses"),
                            new Cell("mods"),
                            new Cell("local pp"),
                            new Cell("weighted pp"),
                            localOrdered.Select(item => new[]
                            {
                                new Cell($"{localOrdered.IndexOf(item) + 1}"),
                                new Cell($"{item.Beatmap.OnlineID} - {item.Beatmap}"),
                                new Cell($"{item.Date:yyyy-MM-dd}"),
                                new Cell($"{item.Combo}/{item.MaxCombo}x") { Align = Align.Right },
                                new Cell($"{Math.Round(item.Accuracy, 2)}%") { Align = Align.Right },
                                new Cell($"{item.MissCount}") { Align = Align.Right },
                                new Cell($"{(item.Mods.Length > 0 ? string.Join(", ", item.Mods) : "None")}") { Align = Align.Right },
                                new Cell($"{item.LocalPP:F1}") { Align = Align.Right },
                                new Cell($"{item.WeightedPP:F1}") { Align = Align.Right }
                            })
                        }
                    })
                );
            }
        }
    }
}
